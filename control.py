#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import SocketServer
import socket
import struct
import subprocess
import threading
import time
from Queue import Queue
import midi_read
import java.util.concurrent.locks.LockSupport
import random

silent = False

PORT = 11526

# def compile_client():
#     if subprocess.call(["make"]):
#         raise Exception("Make failed")

statusChange = None
dieMidiDie = False

def deploy1(machine, n, instr):
    print "deploying on %s..." % machine

    comment_if_silent = '#' if silent else ''

    script = '''
set -e
%ssay -v Mikko 'updating laborgan' || say 'updating lab organ'
rm -f config
printf "%s\n%s\n%d\n" > config
osascript -e 'set volume output volume 75'
''' % (comment_if_silent, socket.gethostbyname(socket.gethostname()), PORT, instr)
    encodedScript = script.replace("'", r"'\''")

    #print "pretended to deploy"
    #return

    if subprocess.call(["ssh", "-o", "StrictHostKeyChecking=no", machine,
                        "sh -c '%s'" % encodedScript]):
        raise Exception("deploy script failed")

    # if subprocess.call(["scp", "organclient", "%s:laborgan/" % machine]):
    #     raise Exception("client binary deploy failed")

    print "deployed on %s." % machine

# def deploy_all(pattern, targets):
#     for t in targets:
#         deploy1(pattern % t, t)

lock = threading.RLock()
connected_clients = []

def launch_client(machine, n):
    def thread_me():
        subprocess.call(["ssh", "-o", "StrictHostKeyChecking=no", machine, "true"])
        if subprocess.call(["ssh", machine,
                            "cd laborgan;" #note the lack of commas, that's intentional
                            "osascript -e 'set volume output volume 75';" +
                            ("" if silent else "say ready;") +
                            "./organclient %d" % n]):
            print "couldn't launch client %d on %s." % (n, machine)
    t = threading.Thread(target=thread_me)
    t.daemon = True
    t.start()

def launch_all(pattern, targets):
    for t in targets:
        launch_client(pattern % t, t)
        java.util.concurrent.locks.LockSupport.parkNanos(1000 * 1000 * 100)

class Client(object):
    def __init__(self, out_queue, client_number):
        self.out_queue = out_queue
        self.client_number = client_number

    def play(self, track, note, velocity):
        self.out_queue.put(struct.pack("<iiii", track, note, velocity, 0))

class TTCPRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        (n,) = struct.unpack("<i", self.request.recv(4))

        out_queue = Queue()

        lock.acquire()
        connected_clients.append(Client(out_queue, n))

        print "client %i connected" % n
        print_status()
        statusChange()
        lock.release()

        while True:
            self.request.sendall(out_queue.get())


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

def run_server():
    ThreadedTCPServer.allow_reuse_address = True
    server = ThreadedTCPServer(('', PORT), TTCPRequestHandler)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()


def launch_lab(instr):
    print "launching laborgan all the way across the lab"
    targets = range(1, 33)
    #pattern = 'lab8-%d.eng.utah.edu'
    pattern = 'daniel-%d'
    deploy1(pattern % targets[0], targets[0], instr)
    run_server()
    launch_all(pattern, targets)
    print "all systems are a go, we have launched"

def print_status():
    lock.acquire()
    up_set = set(c.client_number for c in connected_clients)
    mycoolstring = ""
    for n in range(1, 33):
        row = (n - 1) // 8
        col = (n - 1) % 8
        print (u'🌕' if n in up_set else u'🌑'),
        mycoolstring += (u'🌕' if n in up_set else u'🌑') + " "
        if col == 3:
            print " ",
            mycoolstring += "  "
        if col == 7:
            print
            mycoolstring += "\n"
    lock.release()

def noteToComputer(n):
    # just transpose
    col = (n - 1) // 4
    row = (n - 1) % 4
    return row * 8 + col + 1

def play_note(psuedomidicode, velocity):
    lock.acquire()
    clients_we_want = sorted(connected_clients, key=lambda c:
                                 (abs(c.client_number - psuedomidicode), c.client_number))
    lock.release()
    if clients_we_want:
        clients_we_want[0].play(2, psuedomidicode, velocity)

def play_song(song_num, delay=0):
    lock.acquire()
    c_safe = connected_clients[:]
    lock.release()
    def thread_me():
        for c in c_safe:
            global dieMidiDie
            if dieMidiDie:
                dieMidiDie = False
                return
            c.play(song_num, 0, 0)
            if delay:
                java.util.concurrent.locks.LockSupport.parkNanos(int(1000 * 1000 * 1000 * delay))
    if delay:
        t = threading.Thread(target=thread_me)
        t.daemon = True
        t.start()
    else:
        thread_me() #hah, nonthreaded

def do_evil():
    def thread_me():
        while True:
            global dieMidiDie
            if dieMidiDie:
                dieMidiDie = False
                return
            lock.acquire()
            c = random.choice(connected_clients)
            lock.release()
            c.play(3, 0, 0)
            time.sleep(5)
            c.play(5, 0, 0)
            time.sleep(5)
    t = threading.Thread(target=thread_me)
    t.daemon = True
    t.start()

def play_stereo(song_num):
    lock.acquire()
    for c in connected_clients:
        c.play(song_num, ((c.client_number - 1) // 4) % 2, 0)
    lock.release()

def set_volume(volume):
    lock.acquire()
    for c in connected_clients:
        c.play(0x4, volume, 0)
        #time.sleep(1)
    lock.release()

def kill_afplay():
    lock.acquire()
    for c in connected_clients:
        c.play(0x5, 0, 0)
    lock.release()

def warm_up():
    lock.acquire()
    for c in connected_clients:
        c.play(0x6, 0, 0)
    lock.release()

def play_high():
    lock.acquire()
    for c in connected_clients:
        c.play(0x7, 0, 0)
    lock.release()

def play_midi(in_file):
    def thread_me():
        ppq, tempos, events = midi_read.midi_events_from_file(in_file)
        tempo = tempos[0][1]
        tempos = tempos[1:]
        print ppq

        lock.acquire()
        safe_clients = connected_clients[:]
        lock.release()
        print max(events.keys())
        end = max(events.keys()) + 1
        i = 0
        while i < end:
            for (s, chan, note) in events[i]:
                want = sorted(safe_clients, key=lambda c:
                                  ((chan == 9) ^ (c.client_number == 1),
                              abs(c.client_number + 48 - note), c.client_number))
                if want:
                    want[0].play(2, note - 48, 80 if s else 0)

            diff = 0
            while True:
                i += 1
                diff += 1000 * tempo / ppq
                while tempos and tempos[0][0] <= i:
                    tempo = tempos[0][1]
                    tempos = tempos[1:]
                if not (i < end and not len(events[i])):
                    break

            global dieMidiDie
            if dieMidiDie:
                for c in safe_clients:
                    for n in range(128):
                        c.play(2, n - 48, 0)

                dieMidiDie = False
                return
            java.util.concurrent.locks.LockSupport.parkNanos(diff)
    t = threading.Thread(target=thread_me)
    t.daemon = True
    t.start()

def do_stuff(instr=19, warmup=99):
    def thread_me():
        launch_lab(instr)
        for i in range(5):
            lock.acquire()
            if not silent:
                for c in connected_clients:
                    c.play(warmup, 0, 0)
            lock.release()
            time.sleep(1)
    t = threading.Thread(target=thread_me)
    t.daemon = True
    t.start()

    # lock.acquire()
    # for c in connected_clients:
    #     c.play(2)
    #     time.sleep(1)
    # lock.release()
    # time.sleep(1000000)

if __name__ == "__main__":
    #do_stuff()
    pass
