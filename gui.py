'''
Created on Mar 8, 2013

@author: Daniel
'''
import java.awt
from java.awt import *
from javax.swing import *
from java.awt.event import *
try:
    import com.cobbal.control as control
except:
    import control
import sys
import threading

class myframe(JFrame, ActionListener, KeyListener, FocusListener):
    topPane = JPanel()
    middlePane = JPanel()
    bottomPane = JPanel()
    instructions = JLabel()
    keyboard = ImageIcon("keyboard.png")
    outputArea = JTextArea()
    inputArea = JTextField()

    keymap = {KeyEvent.VK_A:1, KeyEvent.VK_W:2, KeyEvent.VK_S:3,
              KeyEvent.VK_E:4, KeyEvent.VK_D:5, KeyEvent.VK_F:6,
              KeyEvent.VK_T:7, KeyEvent.VK_G:8, KeyEvent.VK_Y:9,
              KeyEvent.VK_H:10, KeyEvent.VK_U:11, KeyEvent.VK_J:12,
              KeyEvent.VK_K:13, KeyEvent.VK_O:14, KeyEvent.VK_L:15,
              KeyEvent.VK_P:16, KeyEvent.VK_SEMICOLON:17, KeyEvent.VK_QUOTE:18
              }

    def __init__(self):
        super(myframe, self).__init__("LABORGAN")
        width = 850
        spacer = 20
        height = 840
        topHeight = 175
        middleHeight = 565
        bottomHeight = 70

        self.setSize(width, height)
        self.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        self.getContentPane().setLayout(BorderLayout())

        control.statusChange = self.statusChange

        # Make small top box which tells you stuff
        self.topPane.setMaximumSize(java.awt.Dimension(width, topHeight))
        self.topPane.setMinimumSize(java.awt.Dimension(width, topHeight))
        self.topPane.setPreferredSize(java.awt.Dimension(width, topHeight))
        self.topPane.setBorder(BorderFactory.createEmptyBorder(spacer, spacer, spacer, spacer))
        self.instructions.setMaximumSize(java.awt.Dimension(width - (2 * spacer), topHeight - (2 * spacer)))
        self.instructions.setMinimumSize(java.awt.Dimension(width - (2 * spacer), topHeight - (2 * spacer)))
        self.instructions.setPreferredSize(java.awt.Dimension(width - (2 * spacer), topHeight - (2 * spacer)))
        self.instructions = JLabel(self.keyboard)
        self.topPane.add(self.instructions)
        self.getContentPane().add(self.topPane, BorderLayout.NORTH)

        # Make large middle box which does stuff
        self.middlePane.setMaximumSize(java.awt.Dimension(width, middleHeight))
        self.middlePane.setMinimumSize(java.awt.Dimension(width, middleHeight))
        self.middlePane.setPreferredSize(java.awt.Dimension(width, middleHeight))
        self.middlePane.setBorder(BorderFactory.createEmptyBorder(spacer, spacer, spacer, spacer))
        self.outputArea.setMargin(Insets(5, 5, 5, 5))
        self.outputArea.setMaximumSize(java.awt.Dimension(width - (2 * spacer), middleHeight - (2 * spacer)))
        self.outputArea.setMinimumSize(java.awt.Dimension(width - (2 * spacer), middleHeight - (2 * spacer)))
        self.outputArea.setPreferredSize(java.awt.Dimension(width - (2 * spacer), middleHeight - (2 * spacer)))
        self.middlePane.add(self.outputArea)
        self.getContentPane().add(self.middlePane, BorderLayout.CENTER)

        # Make a small box which takes input
        self.bottomPane.setMaximumSize(java.awt.Dimension(width, bottomHeight))
        self.bottomPane.setMinimumSize(java.awt.Dimension(width, bottomHeight))
        self.bottomPane.setPreferredSize(java.awt.Dimension(width, bottomHeight))
        self.bottomPane.setBorder(BorderFactory.createEmptyBorder(0, spacer, spacer, spacer))
        self.inputArea.setMaximumSize(java.awt.Dimension(width - (2 * spacer), bottomHeight - (2 * spacer)))
        self.inputArea.setMinimumSize(java.awt.Dimension(width - (2 * spacer), bottomHeight - (2 * spacer)))
        self.inputArea.setPreferredSize(java.awt.Dimension(width - (2 * spacer), bottomHeight - (2 * spacer)))
        self.inputArea.setMargin(Insets(5, 5, 5, 5))
        self.bottomPane.add(self.inputArea)
        self.getContentPane().add(self.bottomPane, BorderLayout.SOUTH)

        self.inputArea.addActionListener(self)
        self.outputArea.addKeyListener(self)
        self.outputArea.setEditable(False)
        self.outputArea.addFocusListener(self)

        self.pack()

    # actionlistener

    def actionPerformed(self, evt):
        text = self.inputArea.getText();
        self.inputArea.text = ""

        if text == "run":
            control.do_stuff()
        elif text == "music":
            control.play_song(1)
        elif text[:4] == "midi":
            control.play_midi(text[5:])
        elif text == "kill":
            control.dieMidiDie = True
            control.kill_afplay()
        elif text == "rr":
            control.play_song(3)
        elif text[:3] == "rrr":
            control.play_song(3, float(text[4:]))
        elif text[:3] == "vol":
            control.set_volume(int(text[4:]))
        elif text == "warm":
            control.warm_up()
        elif text == "special":
            control.play_stereo(8)
        elif text == "high":
            control.play_high()
        elif text == "evil":
            control.do_evil()
        else:
            print "I DON'T KNOW WHAT YOU MEAN!"

    # keylistener

    keys_playing = set() #hacky hack

    def evtToPsuedoMidi(self, evt):
        kc = evt.getKeyCode()
        if kc not in self.keymap:
            return None
        isShift = bool(evt.getModifiers() & InputEvent.SHIFT_DOWN_MASK)
        m = self.keymap[kc]
        return m + (12 if evt.isShiftDown() else 0)

    def keyPressed(self, evt):
        pm = self.evtToPsuedoMidi(evt)
        if (pm is not None) and (pm not in self.keys_playing):
            self.keys_playing.add(pm)
            control.play_note(pm, 80)

    def keyReleased(self, evt):
        pm = self.evtToPsuedoMidi(evt)
        if pm in self.keys_playing:
            self.keys_playing.remove(pm)
        if pm:
            control.play_note(pm - 12, 0)
            control.play_note(pm, 0)
            control.play_note(pm + 12, 0)

    def keyTyped(self, evt):
        pass

    # focuslistener

    def focusGained(self, evt):
        self.outputArea.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color(0x44, 0xaa, 0x44)))

    def focusLost(self, evt):
        self.outputArea.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0))

    writeLock = threading.Lock()
    def write(self, strtoprint):
        self.writeLock.acquire()
        self.outputArea.setText(self.outputArea.getText() + strtoprint)
        self.writeLock.release()

    def flush(self):
        pass

    # write things when things happen to stuff
    def statusChange(self, msg=""):
        control.lock.acquire()

        if msg == "":
            for client in control.connected_clients:
                msg = "%d is connected! :D\n" % client.client_number

        self.outputArea.setText(self.outputArea.getText() + msg)

        control.lock.release()

if __name__ == '__main__':
    control.silent = '--loud' not in sys.argv
    instr = 19
    for a in sys.argv:
        if a[:2] == "-c":
            instr = int(a[2:])
    gui = myframe()
    gui.setVisible(True)
    control.do_stuff(instr, 6 if control.silent else 99)
    #sys.stdout = gui
    #sys.stderr = gui
