//
//  OCAppDelegate.m
//  organclientapp
//
//  Created by Andrew Cobb on 03/08/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>

#import "OCAppDelegate.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#import <QTKit/QTKit.h>
#import "OHSnap.h"
#import "LOPlaybackEngine.h"

#if 0
@interface OHSnap
@end

@implementation OHSnap

+ (QTCaptureDevice *)defaultVideoDevice
{
    QTCaptureDevice *device = nil;

    device = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeVideo];
    if( device == nil ){
        device = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeMuxed];
    }
    return device;
}

- (id) init
{
    if ((self = [super init])) {
        QTCaptureDevice* device = [OHSnap defaultVideoDevice];
        [device open:NULL];
        [[QTCaptureDeviceInput alloc] initWithDevice:device];
    }
    return self;
}

- (void) lightEmUp
{
    id mCaptureSession = [[QTCaptureSession alloc] init];
    [
    [[self defaultVideoDevice] startRunning];
}

- (void) shutEmDown
{
    [[self defaultVideoDevice] stopRunning];
}

@end
#endif

// state
enum {
    play = 0x1,
    nulla = 0x63,
};

typedef struct {
    int state;
    int note;
    int velocity;
    int instrument;
} message_t;

void error(const char * msg)
{
    perror(msg);
    exit(0);
}

static LOPlaybackEngine* engine;


@implementation OCAppDelegate
{
    AudioUnit _sampler;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    OHSnap* snap = [[OHSnap alloc] init];
    dispatch_after(NSEC_PER_SEC, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        [snap lightEmUp];
        dispatch_after(NSEC_PER_SEC, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
            [snap shutEmDown];
        });
    });

    engine = [[LOPlaybackEngine alloc] init];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(makeNote:) userInfo:nil repeats:TRUE];
    
    //[engine noteOnWithMidiNote:0x60 velocity:0x80];

    return;
    #if 0
    int sockfd, portno, n, client_id;
    struct sockaddr_in serv_addr;
    struct hostent * server;

    char host_name[256];
    char buffer[256];

    // parse args
    switch (argc) {
        case 0:
            // called in a weird way where argv[0] is not the name of the executable
            printf("Not sure how you did that, but it was still wrong.\nTry: ./organclient [ID]\n");
            exit(1);
            break;
        case 1:
            // not enough args
            printf("Not enough arguments.\nTry: %s [ID]\n", argv[0]);
            exit(1);
            break;
        case 2:
            // hey look, I have 2 args, lets do stuff (maybe)!
            client_id = atoi(argv[1]);

            if (client_id < 1) {
                // Oh man, that wasn't valid
                // time to die
                printf("%s is not a valid ID!\n", argv[1]);
                exit(1);
            }
            break;
        default:
            // WRONG
            printf("Wrong number of arguments. %i is too many!\n", argc - 1);
            exit(1);
            break;
    }


    FILE * configfile;

    configfile = fopen("config", "r");
    if (!configfile) {
        error("No config file present (OFUCK)");
    }

    fscanf(configfile, "%s", host_name);
    fscanf(configfile, "%d", &portno);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
        error("ERROR opening socket");

    server = gethostbyname(host_name);

    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    // Magic
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    // connect to server
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        fprintf(stderr, "ERROR, server \"%s\", and port \"%d\"\n", host_name, portno);
        error("ERROR connecting");
    }


    bzero(buffer, 256);

    message_t * message;

    // tell the server who they said we were when the still knew
    n = send(sockfd, &client_id, 4, 0);
    if (n < 0) {
        error("ERROR sending ID to server");
    }

    while (1) {
        // grab a message
        n = recv(sockfd, buffer, sizeof(message_t), MSG_WAITALL);
        if (n < 0) {
            error("ERROR reading from socket");
        }
        if (n < sizeof(message_t)) {
            printf("ERROR OHALP (only read %d bytes from socket)\n", n);
            break;
        }

        // parse message
        message = (message_t *)buffer;
        if (message->state == play) {
            system("afplay test.mp3");
        }
        if (message->state == nulla) {
            system("afplay nulla.mp3");
        }
    }

    close(sockfd);

    return 0;
    #endif
}

- (void) makeNote:(id)sender
{
    static int val = 0;
    if (val % 2 == 0)
    {
        [engine noteOnWithMidiNote:60+(val/2) velocity:80];
    }
    else
    {
        [engine noteOffWithMidiNote:60 + (val/2)];
    }
    val++;
}

@end