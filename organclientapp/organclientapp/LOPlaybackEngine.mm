#import "LOPlaybackEngine.h"

#include <algorithm>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#include <pthread.h>

typedef struct
{
    AUGraph graph;
    AudioUnit samplerUnit;
} LOPlaybackEngineRenderState;

#pragma mark -
#pragma mark Private Interface
@interface LOPlaybackEngine ()
- (BOOL) initAudioGraph;
- (void) setupAudioGraph;
- (void) shutdownAudioGraph;
@end

#pragma mark -
@implementation LOPlaybackEngine
{
    NSString* _soundfontFilePath;

    unsigned char _patchIndex;
    
    LOPlaybackEngineRenderState _state;
    
    AUGraph _graph;
    AUNode _outputNode;
    AUNode _samplerNode;
}

#pragma mark Constructors
- (id) init
{
    self = [super init];
    if (self == nil)
        return nil;
    
    _soundfontFilePath = nil;
    
    _graph = NULL;
    _samplerNode = NULL;
    _patchIndex = 0;
    
    _soundfontFilePath = [[NSBundle mainBundle] pathForResource:@"example" ofType:@"sf2"];
    
    BOOL success = [self initAudioGraph];
    if (!success)
        return nil;
    [self setupAudioGraph];
    
    [self setSoundfontPatch:0];
    
    return self;
}

- (void) dealloc
{
    [self shutdownAudioGraph];
}

#pragma mark -
#pragma mark Accessors
- (BOOL) setSoundfontFilePath:(NSString*)filePath
{
    _soundfontFilePath = filePath;
    [self setSoundfontPatch:_patchIndex];
    return TRUE;
}

- (void) setSoundfontPatch:(unsigned char)patchIndex
{
    AUSamplerBankPresetData bpdata;
    bpdata.bankURL = (__bridge CFURLRef)[NSURL fileURLWithPath:_soundfontFilePath];
    bpdata.bankMSB = kAUSampler_DefaultMelodicBankMSB;
    bpdata.bankLSB = kAUSampler_DefaultBankLSB;
    bpdata.presetID = patchIndex;
    
    OSStatus error = AudioUnitSetProperty(_state.samplerUnit,
                                          kAUSamplerProperty_LoadPresetFromBank,
                                          kAudioUnitScope_Global,
                                          0,
                                          &bpdata,
                                          sizeof(bpdata));
    if (error != noErr)
    {
        NSLog(@"Error setting sampler patch index property");
        return;
    }

    _patchIndex = patchIndex;
    return;
}

- (void) noteOnWithMidiNote:(unsigned char)midiNote velocity:(unsigned char)velocity
{
    MusicDeviceMIDIEvent(_state.samplerUnit, 0x91, midiNote, velocity, 0);
}

- (void) noteOffWithMidiNote:(unsigned char)midiNote
{
    MusicDeviceMIDIEvent(_state.samplerUnit, 0x81, midiNote, 0, 0);
}

#pragma mark -
#pragma mark LOPlaybackEngine Methods
- (BOOL) initAudioGraph
{
    OSStatus error = noErr;
    error = NewAUGraph(&_graph);
    if (error != noErr)
        goto _outErr0;
    
    AudioComponentDescription cd;
    
    // Add the output unit
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags = 0;
    cd.componentFlagsMask = 0;
    cd.componentType = kAudioUnitType_Output;
    cd.componentSubType = kAudioUnitSubType_DefaultOutput;
    
    error = AUGraphAddNode(_graph, &cd, &_outputNode);
    if (error != noErr)
        goto _outErr1;
    
    // Add the sampler
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags = 0;
    cd.componentFlagsMask = 0;
    cd.componentType = kAudioUnitType_MusicDevice;
    cd.componentSubType = kAudioUnitSubType_Sampler;
    
    error = AUGraphAddNode(_graph, &cd, &_samplerNode);
    if (error != noErr)
        goto _outErr1;
    
    // Connect the mixer node to the output node
    error = AUGraphOpen(_graph);
    if (error != noErr)
        goto _outErr1;
    error = AUGraphConnectNodeInput(_graph, _samplerNode, 0, _outputNode, 0);
    if (error != noErr)
        goto _outErr1;

    return TRUE;
    
_outErr1:
    DisposeAUGraph(_graph);
_outErr0:
    _graph = NULL;
    NSLog(@"Error initializing audio graph for the first time");
    return FALSE;
}

- (void) setupAudioGraph
{
    _state.graph = _graph;
    
    // Extract I/O and mixer audio units
    AudioUnit outputUnit;
    AudioUnit samplerUnit;
    
    OSStatus error = AUGraphNodeInfo(_graph, _outputNode, NULL, &outputUnit);
    if (error != noErr) NSLog(@"Error extracting output node info");
    error = AUGraphNodeInfo(_graph, _samplerNode, NULL, &samplerUnit);
    if (error != noErr) NSLog(@"Error extracting sampler node info");
    
    Float64 sampleRate = 44100.0;
    
    // Set mixer properties
    // Set the sampler's output sample rate
    error = AudioUnitSetProperty(samplerUnit,
                                 kAudioUnitProperty_SampleRate,
                                 kAudioUnitScope_Output,
                                 0,
                                 &sampleRate,
                                 sizeof(sampleRate));
    if (error != noErr) NSLog(@"Error setting sampler's output sample rate");

    error = AUGraphNodeInfo(_graph, _samplerNode, NULL, &_state.samplerUnit);
    if (error != noErr) NSLog(@"Error extract sampler node info");
    
    error = AUGraphInitialize(_graph);
    if (error != noErr) NSLog(@"Error initializing graph");
    
    error = AUGraphStart(_graph);
    if (error != noErr) NSLog(@"Error starting graph");
    
    CAShow(_graph);
}

- (void) shutdownAudioGraph
{
    AUGraphStop(_graph);
    DisposeAUGraph(_graph);
}

@end

