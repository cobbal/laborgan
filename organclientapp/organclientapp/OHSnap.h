//
//  OHSnap
//  organclientapp
//
//  Created by acobb on 3/11/13.
//  Copyright 2013 Pixio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OHSnap : NSObject
- (void) lightEmUp;
- (void) shutEmDown;
@end