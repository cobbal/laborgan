#import <Foundation/Foundation.h>

@interface LOPlaybackEngine : NSObject

- (BOOL) setSoundfontFilePath:(NSString*)filePath;
- (void) setSoundfontPatch:(unsigned char)patchIndex;
- (void) noteOnWithMidiNote:(unsigned char)midiNote velocity:(unsigned char)velocity;
- (void) noteOffWithMidiNote:(unsigned char)midiNote;

@end
