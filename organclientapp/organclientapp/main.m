//
//  main.m
//  organclientapp
//
//  Created by Andrew Cobb on 03/08/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        NSApplication * application = [NSApplication sharedApplication];
        OCAppDelegate * appDelegate = [[OCAppDelegate alloc] init];
        [application setDelegate:appDelegate];
        [application run];
    }

    return 0;
}