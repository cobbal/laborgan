//
//  OHSnap
//  organclientapp
//
//  Created by acobb on 3/11/13.
//  Copyright 2013 Pixio. All rights reserved.
//

#import "OHSnap.h"
#import <QTKit/QTKit.h>

@implementation OHSnap
{
    QTCaptureDevice* _device;

    QTCaptureDeviceInput* _inp;
    QTCaptureSession* _session;
}


+ (QTCaptureDevice *)defaultVideoDevice
{
    QTCaptureDevice *device = nil;

    device = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeVideo];
    if( device == nil ){
        device = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeMuxed];
    }
    return device;
}

- (id) init
{
    if ((self = [super init])) {
        _device = [OHSnap defaultVideoDevice];
        [_device open:NULL];
        _inp = [[QTCaptureDeviceInput alloc] initWithDevice:_device];
    }
    return self;
}

- (void) lightEmUp
{
    _session = [[QTCaptureSession alloc] init];
    [_session addInput:_inp error:NULL];
    [_session startRunning];
}

- (void) shutEmDown
{
    [_session stopRunning];
    _session = nil;
}

@end