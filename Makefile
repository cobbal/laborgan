#  makefile
CC = clang
CXX = clang++
LDFLAGS = \
	-framework Foundation \
	-framework AVFoundation \
	-framework AudioToolbox \
	-framework AudioUnit \
	-ObjC

CFLAGS = $(FFLAGS) -mmacosx-version-min=10.7 -Wall -g -fobjc-arc
CPPFLAGS = $(CFLAGS)

SOURCES = organclient.c LOPlaybackEngine.mm

all: organclient

organclient: organclient.o LOPlaybackEngine.o
	$(CXX) $(LDFLAGS) -o $@ $^

%.o: %.mm
	$(CXX) $(CPPFLAGS) -c -o $@ $^

clean:
	rm -f organclient *.o
