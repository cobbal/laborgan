#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#import <QTKit/QTKit.h>
#import "LOPlaybackEngine.h"

// state
enum {
    play = 0x1,
    cello = 0x2,
    rr = 0x3,
    volume = 0x4,
    killall = 0x5,
    silence = 0x6,
    highpitch = 0x7,
    special = 0x8,
    nulla = 0x63,
};

typedef struct {
    int state;
    int note;
    int velocity;
    int instrument;
} message_t;

void error(const char * msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char ** argv)
{
    @autoreleasepool {

    int sockfd, portno, instr, n, client_id;
    struct sockaddr_in serv_addr;
    struct hostent * server;

    char host_name[256];
    char buffer[256];

    // parse args
    switch (argc) {
        case 0:
            // called in a weird way where argv[0] is not the name of the executable
            printf("Not sure how you did that, but it was still wrong.\nTry: ./organclient [ID]\n");
            exit(1);
            break;
        case 1:
            // not enough args
            printf("Not enough arguments.\nTry: %s [ID]\n", argv[0]);
            exit(1);
            break;
        case 2:
            // hey look, I have 2 args, lets do stuff (maybe)!
            client_id = atoi(argv[1]);

            if (client_id < 1) {
                // Oh man, that wasn't valid
                // time to die
                printf("%s is not a valid ID!\n", argv[1]);
                exit(1);
            }
            break;
        default:
            // WRONG
            printf("Wrong number of arguments. %i is too many!\n", argc - 1);
            exit(1);
            break;
    }

    FILE * configfile;

    configfile = fopen("config", "r");
    if (!configfile) {
        error("No config file present (OFUCK)");
    }

    fscanf(configfile, "%s", host_name);
    fscanf(configfile, "%d", &portno);
    fscanf(configfile, "%d", &instr);

    LOPlaybackEngine* engine = [[LOPlaybackEngine alloc] initWithMelodic:(client_id != 1) channel:instr];
    if (client_id == 1) {
        engine = nil;
    }


    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
        error("ERROR opening socket");

    server = gethostbyname(host_name);

    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    // Magic
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    // connect to server
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        fprintf(stderr, "ERROR, server \"%s\", and port \"%d\"\n", host_name, portno);
        error("ERROR connecting");
    }


    bzero(buffer, 256);

    message_t * message;

    // tell the server who they said we were when the still knew
    n = send(sockfd, &client_id, 4, 0);
    if (n < 0) {
        error("ERROR sending ID to server");
    }

    while (1) {
        // grab a message
        n = recv(sockfd, buffer, sizeof(message_t), MSG_WAITALL);
        if (n < 0) {
            error("ERROR reading from socket");
        }
        if (n < sizeof(message_t)) {
            printf("ERROR OHALP (only read %d bytes from socket)\n", n);
            break;
        }

        // parse message
        message = (message_t *)buffer;

        switch (message->state) {
            case play:
                system("afplay test.mp3&");
                break;
            case rr:
                system("afplay song.mp3&");
                break;
            case cello:
                if (message->velocity == 0) {
                    [engine noteOffWithMidiNote:message->note+47];
                } else {
                    [engine noteOnWithMidiNote:message->note+47 velocity:message->velocity];
                }
                break;
            case nulla:
                system("afplay nulla.mp3&");
                break;
            case volume:
                system([[NSString stringWithFormat:@"osascript -e 'set volume output volume %d'", message->note] UTF8String]);
                break;
            case killall:
                system("killall afplay");
                break;
            case silence:
                system("afplay silence.mp3&");
                break;
            case special:
                system([[NSString stringWithFormat:@"afplay special-%c.mp3&", (!!message->note)["LR"]] UTF8String]);
                break;
            case highpitch:
                system("afplay high.mp3&");
                break;
        }
    }

    close(sockfd);

    return 0;
    }
}
