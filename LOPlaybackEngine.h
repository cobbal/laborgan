#import <Foundation/Foundation.h>

@interface LOPlaybackEngine : NSObject

- (id) initWithMelodic:(BOOL)melodic channel:(int)chan;

- (BOOL) setSoundfontFilePath:(NSString*)filePath;
- (void) setSoundfontPatch:(unsigned char)patchIndex;
- (void) noteOnWithMidiNote:(unsigned char)midiNote velocity:(unsigned char)velocity;
- (void) noteOffWithMidiNote:(unsigned char)midiNote;

@end
