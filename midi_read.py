from midi.MidiOutStream import MidiOutStream
from midi.MidiInFile import MidiInFile
from collections import defaultdict
from time import time, sleep
from copy import copy
from math import sin, cos, pi, sqrt
import sys

def midi_events_from_file(in_file):
    class NoteOnPrinter(MidiOutStream):
        def __init__(self):
            MidiOutStream.__init__(self)
            self.events = defaultdict(list)
            self.events[0] = []
            self.tempos = []
            self.ticksPerBeat = 480

        def tempo(self, value):
            self.tempos.append((int(self.abs_time()), value))

        def header(self, format, nTracks, division):
            self.ticksPerBeat = division

        def device_name(self, x):
            return None

        def sysex_event(self, *args):
            pass

        def program_name(self, *args):
            pass

        def note_on(self, channel=0, note=0x40, velocity=0x40):
            if channel == 9:
                return
            self.events[self.abs_time()].append((True, channel, note))

        def note_off(self, channel=0, note=0x40, velocity=0x40):
            if channel == 9:
                return
            self.events[self.abs_time()].append((False, channel, note))

    event_handler = NoteOnPrinter()
    #in_file = '/Users/acobb/Desktop/misc/sleeping sun/sleeping sun.mid'
    midi_in = MidiInFile(event_handler, in_file.encode('ascii'))
    midi_in.read()

    events = event_handler.events
    tempos = event_handler.tempos

    return (event_handler.ticksPerBeat, tempos, events)
